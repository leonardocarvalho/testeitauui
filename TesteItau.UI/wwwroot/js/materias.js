﻿$(() => {
    window.materiasUI.InicializarDatatable();
});

window.materiasUI = {
    InicializarDatatable: () => {
        var materiasTable = $('#materias').DataTable({
            "ajax": {
                url: 'https://localhost:44369/api/materia',
                method: "GET",
                "dataSrc": ""
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                { "data": "nome" },
                { "data": "descricao" }
            ],
            "order": [[1, 'asc']]
        });

        $('#materias tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = materiasTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                
                row.child(window.materiasUI.format(row.data().professorMaterias)).show();
                tr.addClass('shown');
            }
        });
    },

    format: d => {
        var professores = '';
        d.forEach(function (x) {
            professores += '<tr>' +
                '<td>Nome:</td>' +
                '<td>' + x.professor.nome + '</td>' +
                '<td>Cpf:</td>' +
                '<td>' + x.professor.cpf + '</td>' +
                '</tr>';
        })

        var childhtml = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
            '<th>Professores:</th>' +
            '</tr>' +
            professores +
            '</table>';

        return childhtml;
    }
},

    window.materiasFunctions = {
        obterMaterias: () => {
            $.ajax({
                url: 'https://localhost:44369/api/materia',
                type: 'get',
                success: function (data) {
                    return data;
                },
                dataType: "application/json"
            });
        },

        renderProfessores: data => {

        }
    }

