﻿$(() => {
    window.professoresUI.InicializarDatatable();
});

window.professoresUI = {
    InicializarDatatable: () => {
        var professoresTable = $('#professores').DataTable({
            "ajax": {
                url: 'https://localhost:44369/api/professor',
                method: "GET",
                "dataSrc": ""
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                { "data": "nome" },
                { "data": "cpf" },
                { "data": "dataNascimento" }
            ],
            "order": [[1, 'asc']]
        });

        $('#professores tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = professoresTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row

                row.child(window.professoresUI.format(row.data().professorMaterias)).show();
                tr.addClass('shown');
            }
        });
    },

    format: d => {
        var materias = '';
        d.forEach(function (x) {
            materias += '<tr>' +
                '<td>Nome:</td>' +
                '<td>' + x.materia.nome + '</td>' +
                '<td>descrição:</td>' +
                '<td>' + x.materia.descricao + '</td>' +
                '</tr>';
        })

        var childhtml = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
            '<th>Materias:</th>' +
            '</tr>' +
            materias +
            '</table>';

        return childhtml;
    }
},

    window.professoresFunctions = {
        obterprofessores: () => {
            $.ajax({
                url: 'https://localhost:44369/api/professor',
                type: 'get',
                success: function (data) {
                    return data;
                },
                dataType: "application/json"
            });
        },

        renderProfessores: data => {

        }
    }

